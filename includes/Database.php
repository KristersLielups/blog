<?php
//include_once "validation.php";

/**
 * Class Database
 *
 * Creates a DB connection and deals with DB operations
 */
class Database{
    // Connection Parameters
    private $DB_HOST        = "localhost";
    private $DB_USER        = "root";
    private $DB_PASS        = "root";
    private $DB_DATABASE    = "blogdatabase";
    private $DB_PORT        = 3306;

    private $connection;

    /**
     * @var PDOStatement
     */
    public $statement;

    /**
     * Database constructor.
     *
     * Creates a connection to the Database using above defined parameters.
     *
     * $options contains additional PDO options
     *
     * Created PDO is stored in $connection
     */
    public function __construct(){
        // Database Source
        $dsn = "mysql:host=" . $this->DB_HOST . ";dbname=" . $this->DB_DATABASE . ";port=" . $this->DB_PORT;
        // Options
        $options = array(
            PDO::ATTR_PERSISTENT            => true,
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_ASSOC
        );

        // Make Connection
        try {
            $this->connection = new PDO($dsn, $this->DB_USER, $this->DB_PASS, $options);
        } catch (PDOException $e){
            die($e->getMessage());
        }
    }

    /**
     * Sends a query to the DB. Results are saved at $statement
     *
     * @param $query
     */
    public function query($query){
        $this->statement = $this->connection->prepare($query);
    }


    public function fetchAll(){
        $this->statement->execute();

        return $this->statement->fetchAll();
    }

    public function fetch(){
        $this->statement->execute();

        return $this->statement->fetch();
    }
}