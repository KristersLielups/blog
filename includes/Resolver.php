<?php
/**
 * Class Resolver
 *
 * Fetches Posts Comments and Users from DB
 */
class Resolver {

    /**
     * @var Database
     */
    private $db;

    /**
     * Resolver constructor.
     */
    public function __construct(){
        $this->db = DatabaseProvider::getConnection();
    }


///////////////////////////
///     Posts
///////////////////////////

/////   Getters         /////

    /**
     * Get all Posts from DB
     *
     * @return array
     */
    public function getPosts(){
        $this->db->query("SELECT p.post_id, p.author_id, u.username, p.title, p.content
                                FROM posts p 
                                LEFT JOIN users u ON p.author_id = u.user_id");

        return $this->db->fetchAll();
    }

    /**
     * Get post by Id
     *
     * @param $postId
     */
    public function getPost($postId){
        $this->db->query("SELECT p.post_id, p.author_id, u.username, p.title, p.content
                                FROM posts p 
                                LEFT JOIN users u ON p.author_id = u.user_id
                                WHERE p.post_id = {$postId}");

        //// TODO Maybe returning an array would be better
        return new Post($this->db->fetch(), $this->getPostComments($postId));
    }

    /**
     * Get all Posts from category
     *
     * @param $category
     * @return array
     */
    public function getPostsFromCategory($category){
        $this->db->query("SELECT p.post_id, p.author_id, u.username, p.title, p.content
                                FROM posts p 
                                INNER JOIN post_has_category phc on p.post_id = phc.post_post_id
                                INNER JOIN categories c on phc.category_category_id = c.category_id
                                LEFT JOIN users u ON p.author_id = u.user_id
                                WHERE c.name = {$category}");

        return $this->db->fetchAll();
    }

    /**
     * Get all Comments on a Post
     *
     * @param int $postId  Post id
     * @return array
     */
    public function getPostComments($postId){
        $this->db->query("SELECT c.content, c.user_id, u.username 
                                FROM comments c
                                INNER JOIN posts p ON c.post_id = p.post_id
                                INNER JOIN users u ON c.user_id = u.user_id
                                WHERE p.post_id = {$postId}");

        return $this->db->fetchAll();
    }

/////   Save            /////

    /**
     * @param Post $post
     */
    public function savePost($post){
        $this->db->query("INSERT INTO posts (title, author_id, content) 
                                VALUES ({$post->getTitle()}, {$post->getAuthorId()}, {$post->getContent()})");
    }

///////////////////////////
///     Users
///////////////////////////

/////   Getters         /////
    /**
     * Finds User by username
     *
     * @param $username
     * @return array
     */
    public function getUserByUsername($username){
        $this->db->query("SELECT * FROM users WHERE username = {$username}");

        return $this->db->fetchAll();
    }

/////   Save            /////

    /**
     * @param User $user
     */
    public function saveUser($user){
        $this->db->query("INSERT INTO users (username, email, password) 
                                VALUES ({$user->getUsername()}, {$user->getEmail()}, {$user->getPassword()})");
    }

///////////////////////////
///     Categories
///////////////////////////

    /**
     * Get all Categories from DB
     *
     * @return array Category data
     */
    public function getCategories(){
        $this->db->query("SELECT * FROM categories");

        return $this->db->fetchAll();
    }
}