<?php

class User
{

    const MIN_USERNAME_LENGTH = 5, MAX_USERNAME_LENGTH = 30;

    private $resolver;
    private $username, $password, $email;

    /**
     * Post constructor.
     */
    public function __construct($details)
    {
        if (empty($details)){
            throw new Exception("Provided detail array is empty!");
        }

        $this->resolver = new Resolver();

        $this->username = $details['username'];
        $this->password = $details['password'];
        $this->email    = $details['email'];
    }

    public function save()
    {
        if ($this->validate()) {
            $this->resolver->saveUser();
        }
    }


///////////////////////////
///     Validation
///////////////////////////

    private function validate()
    {
        if ($this->validateUsername() && $this->validatePassword() && $this->validateEmail()) {

            return true;
        } else {

            return false;
        }
    }

    private function validateUsername(){
        if (strlen($this->username) >= MIN_USERNAME_LENGTH &&
            strlen($this->username) <= MAX_USERNAME_LENGTH &&
            $this->isUsernameAvailable($this->username)){

            return true;
        } else {

            return false;
        }
    }

    private function validatePassword(){
        // TODO validate password
        return true;
    }

    private function validateEmail(){
        if ($this->email == '' || $this->email == null){
            $this->email = null;

            return true;
        } else {
            $emailRegex = "/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x0
                0-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7
            E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x2
            1\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-
                \x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,1
                26}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,
                4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a
                -f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:
            .*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(
                ?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-
                9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD";

            if (preg_match($emailRegex, $this->email)){

                return true;
            } else {

                return false;
            }
        }
    }

    private function isUsernameAvailable($username)
    {
        if (empty($this->resolver->getUserByUsername($username))) {

            return true;
        } else {

            return false;
        }
    }

///////////////////////////
///     Getters and Setters
///////////////////////////

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}