<?php

class Post
{

    private $resolver;
    private /*$id,*/ $title, $authorId, $author, $content, $comments;

    /**
     * Post constructor.
     */
    public function __construct($details, $comments = null)
    {
        if (empty($details)){
            throw new Exception("Provided detail array is empty!");
        }

        $this->resolver = new Resolver();
        //$this->id = $details['post_id'];
        $this->title = $details['title'];
        $this->authorId = $details['author_id'];
        $this->author = $details['username'];
        $this->content = $details['content'];

        if (!empty($comments)) {
            $this->comments = $comments;
        }
    }

    public function save()
    {   // TODO Need to make sure that a user is signed in and get their id
        if ($this->validate()) {
            $this->resolver->savePost();
        }
    }


///////////////////////////
///     Validation
///////////////////////////

    private function validate()
    {
        if (/*is_numeric($this->id) &&*/
            $this->validateUsername($this->username) &&
            $this->title !== '' &&
            $this->content !== '') {

            return true;
        } else {

            return false;
        }
    }

    private function validateUsername($username)
    {
        if (!empty($this->resolver->getUserByUsername($username))) {

            return true;
        } else {

            return false;
        }
    }

///////////////////////////
///     Getters and Setters
///////////////////////////

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * @param mixed $authorId
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return null
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param null $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

}