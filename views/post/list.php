<?php spl_autoload_register(function ($class) {
    include_once "../../includes/" . $class . ".php";
    include_once "../../vendor/autoload.php";
});
error_reporting(E_ALL);
ini_set('display_errors', 1);

$resolver = new Resolver();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>All Posts</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/post/list.js"></script>
</head>
<body>

<!-- Nav Bar -->
<nav class="navbar navbar-dark bg-dark">
    <div class="float-left">
        <a class="navbar-brand" href="list.php">All posts</a>
    </div>
</nav>

<!-- Content -->
<div class="container">
<div class="row">

    <!-- Posts -->
    <div class="col-lg-8">
        <?php
        //$posts = new Easy\Collections\ArrayList();

        foreach ($resolver->getPosts() as $post):
        ?>
        <div class="page-item">
            <a class="title" href="view.php?post_id=<?= $post['post_id'] ?>"><?= $post['title'] ?></a>
            <p><?= $post['content'] ?></p>
<!--            <div>-->
<!--                --><?php //foreach ($resolver->getPostComments($post['post_id']) as $comment): ?>
<!--                <p>--><?//= $comment['username'] . '(' . $comment['user_id'] . ') : ' . $comment['content'] ?><!--</p>-->
<!--                --><?php //endforeach; ?>
<!--            </div>-->
        </div>
        <br>
        <?php
        endforeach;
        ?>
    </div>

    <!-- Sidebar -->
    <div class="col-lg-4">
        <?php
        foreach ($resolver->getCategories() as $category):
        ?>
        <a href="<?= $category['name'] ?>"><?= $category['name'] ?></a>
        <?php
        endforeach;
        ?>
    </div>
</div>
</div>
</body>
</html>
