<?php spl_autoload_register(function ($class) {
    include_once "../../includes/" . $class . ".php";
    include_once "../../vendor/autoload.php";
});
error_reporting(E_ALL);
ini_set('display_errors', 1);

$resolver = new Resolver();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Post</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/post/list.js"></script>
</head>
<body>

<!-- Nav Bar -->
<nav class="navbar navbar-dark bg-dark">
    <div class="float-left">
        <a class="navbar-brand" href="list.php">All posts</a>
    </div>
</nav>

<!-- Content -->
<div class="container">
    <div class="row">

        <!-- Form -->
        <div class="col-lg-12">
            <form>
                <div class="form-group">
                    <label for="Title">Title </label>
                    <input type="text" id="Title" class="form-control" name="title" placeholder="Title" required>
                </div>
                <div class="form-group">
                    <label for="Content">Content </label>
                    <textarea id="Content" class="form-control" name="content" placeholder="Content" required></textarea>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
