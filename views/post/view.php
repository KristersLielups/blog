<?php spl_autoload_register(function ($class) {
    include_once "../../includes/" . $class . ".php";
    include_once "../../vendor/autoload.php";
});
error_reporting(E_ALL);
ini_set('display_errors', 1);

$resolver = new Resolver();

if (!isset($_GET['post_id'])){
    header("Location: list.php");
    die();
}

echo print_r($_GET);
try {
    $post = $resolver->getPost($_GET['post_id']);
} catch (Exception $e){
    header("Location: list.php");
    die();
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Post</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/post/list.js"></script>
</head>
<body>

<!-- Nav Bar -->
<nav class="navbar navbar-dark bg-dark">
    <div class="float-left">
        <a class="navbar-brand" href="list.php">All posts</a>
    </div>
</nav>

<!-- Content -->
<div class="container">
    <div class="row">

        <!-- Post -->
        <div class="col-lg-8">
            <h1><?= $post->getTitle(); ?></h1>
            <h5>Made by: <?= $post->getAuthor(); ?></h5>
            <p><?= $post->getContent(); ?></p>
            <div>
                <?php
                $comments = $post->getComments();
                if (!empty($comments)) {
                    foreach ($comments as $comment): ?>
                        <p><?= $comment['username'] . '(' . $comment['user_id'] . ') : ' . $comment['content'] ?></p>
                    <?php endforeach;
                } else {
                    echo "<h4>There are no comments on this post!</h4>";
                } ?>
            </div>
        </div>

        <!-- Sidebar -->
        <div class="col-lg-4">

        </div>
    </div>
</div>
</body>
</html>
