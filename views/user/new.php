<?php spl_autoload_register(function ($class) {
    include_once "../../includes/" . $class . ".php";
    include_once "../../vendor/autoload.php";
});
error_reporting(E_ALL);
ini_set('display_errors', 1);

$resolver = new Resolver();

if (isset($_POST['submit'])){
    try {
        $user = new User($_POST);
    } catch (Exception $e){

    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Post</title>
    <link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../css/main.css">
    <script src="../../js/jquery-3.3.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/post/list.js"></script>
</head>
<body>

<!-- Nav Bar -->
<nav class="navbar navbar-dark bg-dark">
    <div class="float-left">
        <a class="navbar-brand" href="list.php">All posts</a>
    </div>
</nav>

<!-- Content -->
<div class="container">
    <div class="row">

        <!-- Form -->
        <div class="col-lg-12">
            <form>
                <div class="form-group">
                    <label for="Username">Username </label>
                    <input type="text" id="Username" class="form-control" name="username" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <label for="Password">Password </label>
                    <input type="password" id="Password" class="form-control" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label for="Email">Email </label>
                    <input type="email" id="Email" class="form-control" name="email" placeholder="someone@example.com">
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Sign Up!</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>
